Algoritmo notas
	nota=0
	Escribir "Introduce la nota: "
	leer nota
	Si nota<0 o nota>10 Entonces
		Escribir "Mete notas correctas"
	SiNo
		Si nota>=0 y nota<=4 Entonces
			Escribir "Suspenso"
		FinSi
		Si nota=5 Entonces
			Escribir "Suficiente"
		FinSi
		Si nota=6 Entonces
			Escribir "Bien"
		FinSi
		Si nota>=7 y nota<=8 Entonces
			Escribir "Notable"
		FinSi
		Si nota>=9 y nota<=10 Entonces
			Escribir "Sobresaliente"
		FinSi
	FinSi
FinAlgoritmo
